﻿using Dapper.Contrib.Extensions;
using System;

namespace Bookcase.Integration.Tests.Infrastructure
{
    public partial class DatabaseFixture
    {
        public Book InsertBook(Action<Book> mutator = null)
        {
            var book = new Book
            {
                Author = DataGenerator.Text("Author"),
                Title = DataGenerator.Text("Title"),
            };

            mutator?.Invoke(book);

            book.Id = (int)Connection.Insert(book);

            return book;
        } 
    }
}
