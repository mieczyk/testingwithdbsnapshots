﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Xunit;

namespace Bookcase.Integration.Tests.Infrastructure
{
    public partial class DatabaseFixture : IDisposable
    {
        private readonly IAmDatabaseSnapshot _dbSnapshot;

        public IDbConnection Connection { get; private set; }

        public DatabaseFixture()
        {
            Connection = InitializeTestDbConnection();

            _dbSnapshot = InitializeSnapshot();
            _dbSnapshot.Create();
        }

        private DatabaseSnapshot InitializeSnapshot()
        {
            var masterConnectionString
                = ConfigurationManager.ConnectionStrings["Master"].ConnectionString;

            var testDbName = ConfigurationManager.AppSettings["TestDatabaseName"];

            return new DatabaseSnapshot(testDbName, masterConnectionString);
        }

        private IDbConnection InitializeTestDbConnection()
        {
            var testDbConnectionString
                = ConfigurationManager.ConnectionStrings["Bookcase.Test"].ConnectionString;

            return new SqlConnection(testDbConnectionString);
        }

        public void Restore()
        {
            _dbSnapshot.Restore();
        }

        public void Dispose()
        {
            _dbSnapshot.Delete();
        }
    }

    [CollectionDefinition("DB")]
    public class DatabaseCollection : ICollectionFixture<DatabaseFixture>
    { }
}
