﻿using System;
using System.Data.SqlClient;
using Dapper;
using System.Data;

namespace Bookcase.Integration.Tests.Infrastructure
{
    public interface IAmDatabaseSnapshot
    {
        void Create();
        void Restore();
        void Delete();
    }

    public class DatabaseSnapshot : IAmDatabaseSnapshot
    {
        private readonly string _connectionString;

        public string DatabaseName { get; private set; }
        public string SnapshotName { get; private set; }

        public DatabaseSnapshot(string databaseName, string connectionString)
            : this(
                  databaseName, 
                  $"{databaseName}_snapshot_{DateTime.UtcNow.Ticks}",
                  connectionString
            ) {}

        public DatabaseSnapshot(
            string databaseName, 
            string snapshotName, 
            string connectionString
        )
        {
            DatabaseName = databaseName;
            SnapshotName = snapshotName;
            _connectionString = connectionString;
        }

        public void Create()
        {
            ExecuteStoredProcedure("dbo.sp_create_snapshot", new
            {
                dbName = DatabaseName,
                snapshotName = SnapshotName
            });
        }

        public void Restore()
        {
            ExecuteStoredProcedure("dbo.sp_restore_snapshot", new
            {
                dbName = DatabaseName,
                snapshotName = SnapshotName
            });
        }

        public void Delete()
        {
            ExecuteStoredProcedure("dbo.sp_delete_snapshot", new
            {
                snapshotName = SnapshotName
            });
        }

        private void ExecuteStoredProcedure(string procName, object param)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute(
                    procName,
                    param,
                    commandType: CommandType.StoredProcedure
                );
            }
        }
    }
}
