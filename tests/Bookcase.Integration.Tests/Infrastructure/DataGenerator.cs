﻿using System;
using System.Linq;

namespace Bookcase.Integration.Tests.Infrastructure
{
    public static class DataGenerator
    {
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private readonly static Random rand = new Random();

        public static string Text(string prefix = "", int length = 32)
        {
            var text = new string(
                Enumerable.Repeat(_chars, length)
                    .Select(s => s[rand.Next(s.Length)])
                    .ToArray()
            );

            if(!string.IsNullOrWhiteSpace(prefix))
            {
                text = $"{prefix}_{text}";
            }

            return text;
        }

        public static int Number(
            int min = int.MinValue, 
            int max = int.MaxValue
        )
        {
            return rand.Next(min, max);
        }
    }
}
