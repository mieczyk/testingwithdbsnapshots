﻿using Xunit;

namespace Bookcase.Integration.Tests
{
    public class BooksWriterTests
    {
        [Fact]
        public void stores_given_book_in_db()
        {
            Assert.True(false);
        }

        [Fact]
        public void updates_given_book()
        {
            Assert.True(false);
        }

        [Fact]
        public void throws_exception_if_book_to_update_does_not_exist()
        {
            Assert.True(false);
        }

        [Fact]
        public void removes_book_with_given_id()
        {
            Assert.True(false);
        }

        [Fact]
        public void throws_exception_if_book_to_delete_does_not_exist()
        {
            Assert.True(false);
        }
    }
}
