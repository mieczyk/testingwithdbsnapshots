﻿using Bookcase.Integration.Tests.Infrastructure;
using System;
using System.Linq;
using Xunit;

namespace Bookcase.Integration.Tests
{
    [Collection("DB")]
    public class BooksReaderTests : IDisposable
    {
        private readonly DatabaseFixture _db;
        private readonly BooksReader _reader;

        public BooksReaderTests(DatabaseFixture db)
        {
            _db = db;
            _reader = new BooksReader(_db.Connection);
        }

        [Fact]
        public void returns_paginated_list_of_books()
        {
            // Arrange
            const int booksCount = 10;

            for (int i = 0; i < booksCount; i++)
            {
                _db.InsertBook();
            }

            // Act
            var booksList = _reader.GetAll(1, 5);

            // Assert
            Assert.Equal(booksCount, booksList.Total);
            Assert.Equal(5, booksList.Items.Count());
        }

        [Fact]
        public void returns_empty_list_if_no_book_is_stored_in_db()
        {
            // Act
            var booksList = _reader.GetAll(1, 5);

            // Assert
            Assert.Empty(booksList.Items);
            Assert.Equal(0, booksList.Total);
        }

        [Fact]
        public void returns_book_with_given_id()
        {
            // Arrange
            var expectedBook = _db.InsertBook();

            _db.InsertBook();
            _db.InsertBook();

            // Act
            var book = _reader.Get(expectedBook.Id);

            // Assert
            Assert.Equal(expectedBook.Id, book.Id);
        }

        [Fact]
        public void returns_null_if_book_with_given_id_does_not_exist()
        {
            // Act
            var book = _reader.Get(DataGenerator.Number());

            // Assert
            Assert.Null(book);
        }

        public void Dispose()
        {
            _db.Restore();
        }
    }
}
