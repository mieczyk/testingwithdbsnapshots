﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using SimpleMigrations;
using SimpleMigrations.Console;
using SimpleMigrations.DatabaseProvider;

namespace Bookcase.Migrations.Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!ValidateArguments(args))
            {
                DisplayApplicationUsage();
                Environment.Exit(1);
            }

            var connectionString = GetConnectionString(args[0]);

            using (var connection = new SqlConnection(connectionString))
            {
                var dbProvider = new MssqlDatabaseProvider(connection);
                var migrator = new SimpleMigrator(FindMigrationsAssembly(), dbProvider);

                var runner = new ConsoleRunner(migrator);
                runner.Run(args.Skip(1).ToArray());
            }
        }

        private static bool ValidateArguments(IReadOnlyCollection<string> args)
        {
            return args.Count >= 2;
        }

        private static void DisplayApplicationUsage()
        {
            Console.WriteLine("Migrations runner usage:");
            Console.WriteLine($"\t{AppDomain.CurrentDomain.FriendlyName} <TARGET> <OPERATION>");
            Console.WriteLine("<TARGET> = Bookcase | Bookcase.Test");
            Console.WriteLine("<OPERATION> = up | to | reapply | list | baseline");
        }

        private static Assembly FindMigrationsAssembly()
        {
            return typeof (MigrationsMarker).Assembly;
        }

        private static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}
