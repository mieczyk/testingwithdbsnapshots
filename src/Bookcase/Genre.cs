﻿namespace Bookcase
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }
}
