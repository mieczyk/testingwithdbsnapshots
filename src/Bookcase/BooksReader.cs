﻿using Dapper;
using System.Data;
using System.Linq;

namespace Bookcase
{
    public class BooksReader
    {
        private readonly IDbConnection _connection;

        public BooksReader(IDbConnection connection)
        {
            _connection = connection;
        }

        public BooksList GetAll(int page, int size)
        {
            var sql = @"
                SELECT COUNT(1) FROM dbo.Books;
                SELECT * FROM dbo.Books ORDER BY Id OFFSET @offset ROWS FETCH NEXT @size ROWS ONLY;";

            var param = new { offset = page - 1, size = size };

            using (var multi = _connection.QueryMultiple(sql, param))
            {
                var total = multi.Read<int>().Single();
                var books = multi.Read<Book>().ToList();

                return new BooksList(books, total);
            }
        }

        public Book Get(int id)
        {
            return _connection.Query<Book>(
                @"SELECT * FROM dbo.Books WHERE Id = @id", 
                new { id }
            ).FirstOrDefault();
        }
    }
}
