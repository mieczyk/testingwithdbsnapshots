﻿namespace Bookcase
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Isbn { get; set; }
        public int? GenreId { get; set; }
    }
}
