﻿using System.Collections.Generic;

namespace Bookcase
{
    public class BooksList
    {
        public IEnumerable<Book> Items { get; private set; }
        public int Total { get; private set; }

        public BooksList(IEnumerable<Book> items, int total)
        {
            Items = items;
            Total = total;
        }
    }
}
