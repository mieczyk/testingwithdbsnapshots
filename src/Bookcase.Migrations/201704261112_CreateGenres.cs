﻿using SimpleMigrations;

namespace Bookcase.Migrations
{
    [Migration(201704261112)]
    public class _201704261112_CreateGenres : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.Genres(
    Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    Name NVARCHAR(256) NOT NULL,
    DisplayName NVARCHAR(512) NOT NULL
)
            ");
        }

        public override void Down()
        {
            Execute(@"DROP TABLE dbo.Genres");
        }
    }
}
