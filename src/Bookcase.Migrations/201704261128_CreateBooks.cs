﻿using SimpleMigrations;

namespace Bookcase.Migrations
{
    [Migration(201704261128)]
    public class _201704261128_CreateBooks : Migration
    {
        public override void Up()
        {
            Execute(@"
CREATE TABLE dbo.Books(
    Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    Title NVARCHAR(512) NOT NULL,
    Author NVARCHAR(512) NOT NULL,
    Isbn VARCHAR(20) NULL,
    GenreId INT NULL FOREIGN KEY REFERENCES dbo.Genres(Id) 
)
            ");
        }

        public override void Down()
        {
            Execute(@"DROP TABLE dbo.Books");
        }
    }
}
