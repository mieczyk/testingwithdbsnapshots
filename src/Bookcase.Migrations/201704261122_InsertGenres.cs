﻿using System;
using SimpleMigrations;

namespace Bookcase.Migrations
{
    [Migration(201704261122)]
    public class _201704261122_InsertGenres : Migration
    {
        public override void Up()
        {
            Execute(@"
INSERT INTO dbo.Genres(Name, DisplayName) VALUES
    (N'History', N'History'),
    (N'Fantasy', N'Fantasy'),
    (N'ScienceFiction', N'Science Fiction'),
    (N'Science', N'Science')
            ");
        }

        public override void Down()
        {
            Execute(@"TRUNCATE TABLE dbo.Genres");   
        }
    }
}
