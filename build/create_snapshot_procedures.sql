USE [master]
GO

-- Define procedure that creates a DB snapshot.
IF OBJECT_ID('dbo.sp_create_snapshot') IS NULL
	EXEC('CREATE PROCEDURE dbo.sp_create_snapshot AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE dbo.sp_create_snapshot
	@dbName SYSNAME,
	@snapshotName SYSNAME
AS
	DECLARE @snapshotsDir NVARCHAR(512) = 'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLSERVER\MSSQL\repldata\';
	DECLARE @snapshotFileName NVARCHAR(1024) = @snapshotsDir + @snapshotName + '.mdf'

	DECLARE @sql VARCHAR(MAX) = 
		'CREATE DATABASE ' + @snapshotName + 
		' ON (NAME=' + @dbName + ', FILENAME=''' + @snapshotFileName +  ''')' 
		+ ' AS SNAPSHOT OF ' + @dbName

	EXEC(@sql)
GO

-- Define procedure that restores a DB snapshot.
IF OBJECT_ID('dbo.sp_restore_snapshot') IS NULL
	EXEC('CREATE PROCEDURE dbo.sp_restore_snapshot AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE dbo.sp_restore_snapshot
	@dbName SYSNAME,
	@snapshotName SYSNAME
AS
	EXEC('ALTER DATABASE ' + @dbName + ' SET SINGLE_USER WITH ROLLBACK IMMEDIATE');
	EXEC('RESTORE DATABASE ' + @dbName +  ' FROM DATABASE_SNAPSHOT = ''' + @snapshotName + '''');
	EXEC('ALTER DATABASE ' + @dbName +  ' SET MULTI_USER');
GO

-- Define procedure that removes a DB snapshot.
IF OBJECT_ID('dbo.sp_delete_snapshot') IS NULL
	EXEC('CREATE PROCEDURE dbo.sp_delete_snapshot AS SET NOCOUNT ON;')
GO

ALTER PROCEDURE dbo.sp_delete_snapshot
	@snapshotName SYSNAME
AS
	EXEC('DROP DATABASE ' + @snapshotName);
GO